resource_name :ocserv_config
property :name, String, name_property: true, identity: true
property :value, [String, Array], required: true
default_action :create

action_class do
  include Ocserv::Config
end

load_current_value do
  extend Ocserv::Config
  val = config_value(name, node)
  value val unless val.nil?
  current_value_does_not_exist! if val.nil?
end

action :create do
  converge_if_changed do
    replace_or_add_config(name, value, node)
    log 'Queuing restart on service[ocserv] due to config change.' do
      notifies :restart, 'service[ocserv]' unless node['ocserv']['config']['ipv4-network'].nil? && node['ocserv']['config']['ipv6-network'].nil?
    end
  end
end
